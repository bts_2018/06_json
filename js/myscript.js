$( document ).ready(function() {
  

	//AJAX LOAD DATA
	var country = $('#country');
	$.ajax({  
        url: 'json/countries.json', 
        dataType: 'json',
        success: function (items) { 
            $.each(items.countries, function(key, val) {
				country.append('<option value="' + val.code + '">' + val.name + '</option>');
			})
        },
        error: function(){
    		alert('error on load country.json');
  		}
	});

	 

	//AJAX LOAD CATEGORIES
	var categories = $('#categories');
	 $.ajax({  
        url: 'json/categories.json', 
        dataType: 'json',
        success: function (items) { 
            $.each(items.categories, function(key, val) {
				categories.append('<h1>' + key + " " +  val.name + '</h1>');
			})
        },
        error: function(){
    		console.log('error on load categories.json');
  		}
	});

 

});

 

 